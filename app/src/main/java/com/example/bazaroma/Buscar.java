package com.example.bazaroma;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class Buscar extends AppCompatActivity {
    private Button btnScanner;
    private TextView tvBarCode;
    private EditText et_codigo, et_descripcion, et_precio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);
        et_codigo = (EditText) findViewById(R.id.et_CodigoB);
        et_descripcion = (EditText) findViewById(R.id.et_descripcionB);
        et_precio = (EditText) findViewById(R.id.et_precioB);
        btnScanner = findViewById(R.id.btnScanner);
        tvBarCode = findViewById(R.id.tvBarCode);
        btnScanner.setOnClickListener(mOnClickListener);

    }

    public void Buscar(View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "adminisracion",null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();

        String codigo = et_codigo.getText().toString();
        Cursor fila = BaseDeDatos.rawQuery("select description, precio from articulos where codigo ="+codigo, null);
        if(fila.moveToFirst()){
            et_descripcion.setText(fila.getString(0));
            et_precio.setText(fila.getString(1));
            BaseDeDatos.close();
        }else{
            Toast.makeText(this,"No se registro el articulo",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result!=null)
            if(result.getContents() != null){
                tvBarCode.setText("El codigo de barra es:\n" + result.getContents());
                et_codigo.setText(result.getContents());

            }else{
                tvBarCode.setText("Error al escanear el codigo de barra");
            }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            switch (v.getId()) {
                case R.id.btnScanner:
                    new IntentIntegrator(Buscar.this).initiateScan();
                    break;
            }
        }
    };
    public void Regresar(View view){
        Intent regresar = new Intent(this, MainActivity.class);
        startActivity(regresar);
        finish();
    }
}