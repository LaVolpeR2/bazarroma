package com.example.bazaroma;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

public class Venta extends AppCompatActivity implements View.OnClickListener {
    private EditText et_codigo, et_descripcion, et_precio;
    private String codigoP, descripcionP,precioP;
    private Button btnScanner,btnAñadir;
    private ListView list;
    private ArrayList<String> names;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venta);
        et_codigo = (EditText) findViewById(R.id.et_CodigoV);
        et_descripcion = (EditText) findViewById(R.id.et_DescripcionV);
        et_precio = (EditText) findViewById(R.id.et_PrecioV);
        btnScanner = findViewById(R.id.btnScanner);
        btnScanner.setOnClickListener(mOnClickListener);
        btnAñadir = findViewById(R.id.btnAgregarV);
        list = (ListView) findViewById(R.id.list);
        names = new ArrayList<String>();
    }


    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "adminisracion",null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        if(result!=null)
            if(result.getContents() != null){
                et_codigo.setText(result.getContents());
                String codigo = et_codigo.getText().toString();
                Cursor fila = BaseDeDatos.rawQuery("select description, precio from articulos where codigo ="+codigo, null);
                if(fila.moveToFirst()){
                    et_descripcion.setText(fila.getString(0));
                    et_precio.setText(fila.getString(1));

                    BaseDeDatos.close();
                }else{
                    Toast.makeText(this,"No se registro el articulo",Toast.LENGTH_SHORT).show();
                }
            }else{
                et_codigo.setText("error");
            }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            switch (v.getId()) {
                case R.id.btnScanner:
                    new IntentIntegrator(Venta.this).initiateScan();
                    break;
            }
        }
    };

    public void Regresar(View view){
        Intent regresar = new Intent(this, MainActivity.class);
        startActivity(regresar);
        finish();
    }

    public void Añadir(View view){
        codigoP = et_codigo.getText().toString();
        descripcionP = et_descripcion.getText().toString();
        precioP = et_precio.getText().toString();
        names.add(descripcionP);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        list.setAdapter(adapter);
        Toast.makeText(this, "Codigo: "+codigoP + " Precio: "+ precioP + " Descripcion: "+descripcionP, Toast.LENGTH_SHORT).show();
    }
    public void GenerarBoleta(){

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAgregarV:
                String textoa = et_codigo.getText().toString().trim();
                String textob = et_descripcion.getText().toString().trim();
                String textoc = et_precio.getText().toString().trim();
        }
    }
}