package com.example.bazaroma;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //Metodo Boton Venta
    public void Venta(View view){
        Intent venta = new Intent(this, Venta.class);
        startActivity(venta);
        finish();
    }
    public void Buscar(View view){
        Intent buscar = new Intent(this, Buscar.class);
        startActivity(buscar);
        finish();
    }
    public void Registrar(View view) {
        String codigo = "4897069495252";
        String descripcion = "CAT";
        String precio = "200";
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "adminisracion", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029465625";
        descripcion = "camisa m/l";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39379351";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029465618";
        descripcion = "camisa m/l";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100028744653";
        descripcion = "bluza m/l";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "5143192001";
        descripcion = "bluza m/l";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "5142890002";
        descripcion = "jeans";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "5142899002";
        descripcion = "jeans";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100028727267";
        descripcion = "jeans";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029465953";
        descripcion = "poleron cap.";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029872683";
        descripcion = "pantalon for";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100028746732";
        descripcion = "jeans";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "9060001406066";
        descripcion = "chompa,hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "90220753";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "6860015639473";
        descripcion = "pantufla";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "5296413580061";
        descripcion = "polo m/l";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "50100750";
        descripcion = "chompa,hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39428394";
        descripcion = "gorrito";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39423009";
        descripcion = "almohada";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39408556";
        descripcion = "tuto muñe";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39408501";
        descripcion = "tuto muñe";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39408334";
        descripcion = "babero sili";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39408327";
        descripcion = "manta m/u";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39408167";
        descripcion = "babero sili";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39407160";
        descripcion = "toalla /capu";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39407139";
        descripcion = "babero sili";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39407078";
        descripcion = "toalla /capu";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39406958";
        descripcion = "toalla /capu";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39406903";
        descripcion = "manta m/u";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39406859";
        descripcion = "babero x2";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39406804";
        descripcion = "toalla /capu";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39406798";
        descripcion = "toalla /capu";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39404848";
        descripcion = "jeans";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39403803";
        descripcion = "dril estreh";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39403643";
        descripcion = "jeans";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39403544";
        descripcion = "chomp carita";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401984";
        descripcion = "polera cap";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401717";
        descripcion = "jeans";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401502";
        descripcion = "chompa,hilo";
        precio = "33";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401496";
        descripcion = "chompa,hilo";
        precio = "33";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401489";
        descripcion = "chompa,hilo";
        precio = "33";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401465";
        descripcion = "chompa,hilo";
        precio = "33";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401311";
        descripcion = "cafarena";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401052";
        descripcion = "leggin";
        precio = "23";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39401038";
        descripcion = "leggin";
        precio = "23";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39400932";
        descripcion = "leggin";
        precio = "23";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39380395";
        descripcion = "poleron cap.";
        precio = "45";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39379344";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39379313";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39379290";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39379283";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39378941";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39377494";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39377470";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39377456";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39336378";
        descripcion = "set cubierto";
        precio = "5";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39336200";
        descripcion = "set de plato bb";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39336194";
        descripcion = "set de plato bb";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39293916";
        descripcion = "gorrito";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39287311";
        descripcion = "set cubierto";
        precio = "5";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39287298";
        descripcion = "vaso";
        precio = "10";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39287281";
        descripcion = "set cubierto";
        precio = "5";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39287229";
        descripcion = "set cubierto";
        precio = "5";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39287175";
        descripcion = "set cubierto";
        precio = "5";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39237446";
        descripcion = "traje de baño";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39237293";
        descripcion = "traje de baño";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39237064";
        descripcion = "traje de baño";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "39237019";
        descripcion = "traje de baño";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "3000805406059";
        descripcion = "chompa,hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016539581";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016539567";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016539529";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016539468";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016539437";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016539383";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016539376";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016465248";
        descripcion = "polo m/c";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016465224";
        descripcion = "polo m/c";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016465217";
        descripcion = "polo m/c";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016460335";
        descripcion = "polo m/c";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016460328";
        descripcion = "polo m/c";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016435401";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016435395";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016435166";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016435050";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016435043";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016435036";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016434985";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016413775";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016413621";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016316762";
        descripcion = "poli/vestido";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016152247";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016152216";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016152087";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016151950";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800016151912";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015792215";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015792130";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015774013";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015773863";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015639657";
        descripcion = "pantufla";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015615385";
        descripcion = "leggin";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015615255";
        descripcion = "leggin";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015613923";
        descripcion = "polera cap";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015613367";
        descripcion = "micro polar";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015613152";
        descripcion = "micro polar";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015612759";
        descripcion = "micro polar";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015612605";
        descripcion = "micro polar";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015612568";
        descripcion = "micro polar";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015612544";
        descripcion = "micro polar";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015576372";
        descripcion = "buzo compl";
        precio = "50";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015559610";
        descripcion = "micro polar";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015536598";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015535669";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015535607";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015535546";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015535423";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015535362";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015535300";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015517030";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015516965";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015516903";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015516712";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015516590";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015515944";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015515319";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015515302";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015515289";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015515272";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015515128";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015515111";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015514732";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015514725";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015514701";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015514664";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015514602";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015514572";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015514534";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015513988";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015513940";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015513629";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015442974";
        descripcion = "chompa/raya";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015442912";
        descripcion = "chompa/raya";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015350408";
        descripcion = "polo m/l";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015350323";
        descripcion = "polo m/l";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2800015341857";
        descripcion = "polera cap";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2550043392028";
        descripcion = "camioneta friccion";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030327547";
        descripcion = "bluza m/l";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030304210";
        descripcion = "buzo compl";
        precio = "85";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030295303";
        descripcion = "chompa,hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030266778";
        descripcion = "polera,cap";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030261070";
        descripcion = "bluza m/l";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030253419";
        descripcion = "chompa,hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030247371";
        descripcion = "chompa";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030242000";
        descripcion = "bluza m/l";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030241997";
        descripcion = "bluza, flori";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100030058809";
        descripcion = "chompa piel";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029917452";
        descripcion = "poleron";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029881920";
        descripcion = "chompa";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029881906";
        descripcion = "polera";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029872676";
        descripcion = "pantalon for";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029594868";
        descripcion = "leggin";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029594844";
        descripcion = "leggin";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029562126";
        descripcion = "chompa,hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029469845";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029469807";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029469746";
        descripcion = "polera cap";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029469647";
        descripcion = "polera";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029469517";
        descripcion = "leggin";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029469449";
        descripcion = "jeans";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029466042";
        descripcion = "poleron cap.";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029465960";
        descripcion = "poleron cap.";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029465946";
        descripcion = "polera";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029465922";
        descripcion = "polera";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029432412";
        descripcion = "polera";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100029432405";
        descripcion = "polera";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2100022858837";
        descripcion = "leggin";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2050043391700";
        descripcion = "auto bloques";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "205004173093";
        descripcion = "set 3 carros";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2050004249637";
        descripcion = "peluches";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2050004231854";
        descripcion = "carro";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2050003735216";
        descripcion = "tractor granja";
        precio = "40";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "2033235195348";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "1800485515247";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "1524790517153";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "151900000680";
        descripcion = "leggin";
        precio = "30";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "1427264970068";
        descripcion = "polo m/l";
        precio = "25";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "10883419";
        descripcion = "bluza,floria";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "100005402488";
        descripcion = "chompa, hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "1000005406042";
        descripcion = "chompa, hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "1000005402457";
        descripcion = "chompa, hilo";
        precio = "35";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "075290000085";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "075290000085";
        descripcion = "polo m/c";
        precio = "15";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "05421282";
        descripcion = "camion /varios";
        precio = "45";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        codigo = "029000000476";
        descripcion = "polo m/l";
        precio = "20";
        registro.put("codigo", codigo);
        registro.put("description", descripcion);
        registro.put("precio", precio);
        BaseDeDatos.insert("articulos", null, registro);
        //
        BaseDeDatos.close();
        Toast.makeText(this,"Se registro el articulos",Toast.LENGTH_SHORT).show();
    }
}